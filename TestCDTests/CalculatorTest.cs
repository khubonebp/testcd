﻿using NUnit.Framework;
using TestCD;

namespace TestCDTests
{
    [TestFixture]
    public class CalculatorTest
    {
        [Test]
        public void TestAdd()
        {
            var cal = new Calculator();
            Assert.AreEqual(cal.Add(1,1),2);
        }

        [Test]
        public void TestSubtract()
        {
            var cal = new Calculator();
            Assert.AreEqual(cal.Subtract(1, 1), 0);
        }
    }
}
