﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCD
{
    public class Calculator
    {
        public int Add(int leftOperand,int rightOperand)
        {
            return leftOperand + rightOperand;
        }

        public int Subtract(int leftOperand,int rightOperand)
        {
            return leftOperand - rightOperand;
        }
    }
}
